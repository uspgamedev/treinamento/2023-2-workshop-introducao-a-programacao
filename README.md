# Workshop de Introdução à Programação com GDScript

Esse material é um fork do original produzido por Pedro Schneider sob a Licença
CC-BY-SA 4.0. A principal mudança aqui é a inclusão de um demo bem simples para
fazer exemplos visuais dos conceitos passados. Veja [aqui](sandbox/README.md)
para mais informações.

## Sumário

- [O que é um código (script)](#o-que-é-um-código-script)

- [Conceitos básicos de programação](#conceitos-básicos)
  
  - [Impressão de dados para o console](#impressão-de-dados-para-o-console)
  
  - [Variáveis](#variáveis)
  
  - [Constantes](#constantes)
  
  - [Enumeradores](#enumeradores)
  
  - [Operadores](#operadores)

- [Controle de Fluxo](#controle-de-fluxo)
  
  - [Condicional](#condicional)
  
  - [Enquanto](#enquanto)
  
  - [Iterador](#iterador)
  
  - [Correspondência](#correspondência)

- [Funções](#funções)

- [Escopo](#escopo)

- [Classes](#classes)
  
  - [Construtor](#construtor)
  
  - [Métodos](#métodos)
  
  - [Herança](#herança)

- [Conteúdo extra para estudo](#conteúdo-extra-para-estudo)

## O que é um código (script)?

- Códigos (scripts) são sequências de instruções

- Geralmente são interpretados de cima para baixo
  
  - No entanto, é possível controlar esse fluxo de algumas formas

- O computador vai fazer exatamente o que você pedir para ele fazer; nada mais, nada menos 
  
  - As instruções que damos a um computador precisam ser muito bem pensadas para não causar erros

## Conceitos básicos

### Impressão de dados para o console

- Podemos pedir para o nosso programa imprimir alguma coisa no console:

```gdscript
extends Node

func _ready() -> void:
    print("Olá, mundo!");
```

```
Output:
Olá mundo
```

### Variáveis

- Blocos de memória que guardam algum dado
  
  - Memória são apenas 0s e 1s
  
  - Precisamos dizer ao programa como essa memória deve ser interpretada (tipos de dados)
  
  - Podemos dar qualquer nome que quisermos para variáveis
    
    - É importante que o nome que escolhermos indique
  
  - Também podemos pedir para o nosso programa imprimir variáveis no console

- Tipos básicos de dados:
  
  - `int`: Representa um número inteiro
  
  - `float`: Representa um número de ponto flutuante (número real)
  
  - `char`: Representa um caractere (não existe em GDScript)
  
  - `bool`: Representa um valor verdadeiro ou falso

```gdscript
extends Node

var numero_inteiro: int = 100; # Esse não é um nome bom
var numero_decimal: float = 10000.0; # Esse também não é um nome bom
var eh_legal: bool = true;

func _ready() -> void:
    print(numero_inteiro);
    print(numero_decimal);
    print(eh_legal);
```

```
Output:
100
10000
Batata
True
```

- Outros tipos de dados:
  
  - `String`: Representa um conjunto de caracteres
  
  - `Array`: Representa uma lista de objetos
  
  - `Dictionary`: Representa uma coleção de pares chave-valor
  
  - `Vector2`: Representa um vetor em 2D
  
  - `Vector3`: Representa um vetor em 3D

```gdscript
extends Node

var batata: String = "Batata";
var nomes: Array = ["Pedro", "João", "Julia", "Maria"];
var dicionario: Dictionary = {12543328: "Pedro Schneider", "Professora": "Lúcia"};
var posicao2D: Vector2 = Vector2(100, 10);
var posicao3D: Vector3 = Vector3(100, 200, 50);

func _ready() -> void:
    # String
    print(batata);
    print();

    # Array
    print(nomes);
    print(nomes[2]);
    print();

    # Dictionary
    print(dicionario);
    print(dicionario[12543328]);
    print(dicionario["Professora"]);
    # Adicionando entradas no dicionário
    dicionario.professor_auxiliar = "José";
    dicionario[12543139] = "Maria Clara";
    print(dicionario.professor_auxiliar);
    print(dicionario[12543139]);
    print();

    # Vector2
    print(posicao2D);
    print(posicao2D.x);
    print(posicao2D.y);
    print();

    # Vector3
    print(posicao3D);
    print(posicao3D.x);
    print(posicao3D.y);
    print(posicao3D.z);
```

```
Output:
Batata

[Pedro, João, Julia, Maria]
Julia

{12543328:Pedro Schneider, Professora:Lúcia}
Pedro Schneider
Lúcia
José
Maria Clara
{12543139:Maria Clara, 12543328:Pedro Schneider, Professora:Lúcia, professor_auxiliar:José}

(100, 10)
100
10

(100, 200, 50)
100
200
50
```

- Conversão de tipos:
  - Podemos converter dados de um tipo para outro, caso essa conversão esteja definida, fazendo uso da palavra chave `as`

```gdscript
extends Node

func _ready() -> void:
    var numero = "123" as int;
    print(numero);
    print(numero is String);
    print(numero is int);
    print();

    var palavra = 123 as String;
    print(palavra);
    print(palavra is int);
    print(palavra is String);
```

```
Output:
123
False
True

123
False
True
```

### Constantes

- Blocos de memória semelhantes a variáveis

- Possuem tipos de dados assim como variáveis

- A diferença é que são imutáveis
  
  - Não podem ser modificadas

```gdscript
extends Node

const NOME: String = "Pedro Schneider";
const ANIVERSARIO: String = "29/10/2001";
const GRAVIDADE: float = 9.8;


func _ready() -> void:
    print(NOME);
    print(ANIVERSARIO);
    print(GRAVIDADE);
```

```
Output:
Pedro Schneider
29/10/2001
9.8
```

- O que acontece se tentarmos modificar uma constante?

```gdscript
extends Node

const NOME: String = "Pedro Schneider";

func _ready() -> void:
    NOME = "João Carlos";
```

```
Mensagem de erro:
error(6,1): Can't assign a new value to a constant.
```

### Enumeradores

- Representam uma série de possibilidades definidas

- São úteis em problemas de classificação, por exemplo
  
  - Ao invés de representarmos classes diferentes com `String`s ou `int`s, podemos usar enumeradores

- São mapeados para inteiros

```gdscript
extends Node

enum {
    NEUTRO,
    INIMIGO,
    ALIADO
}

enum EnumComNome {
    TIPO_1,
    TIPO_2,
    TIPO_3,
    INVALIDO = -1
}

func _ready() -> void:
    var entidade1: int = NEUTRO;
    var entidade2: int = INIMIGO;

    print(entidade1);
    print(entidade2);
    print(ALIADO);

    print();

    print(EnumComNome.TIPO_1);
    print(EnumComNome.TIPO_2);
    print(EnumComNome.TIPO_3);
    print(EnumComNome.INVALIDO);
```

```
Output:
0
1
2

0
1
2
-1
```

### Operadores

- Formas de manipular os dados em uma variável

- A funcionalidade dos operadores pode variar dependendo de quais tipos de dados são utilizados neles

- Operadores matemáticos:
  
  - `+`: Soma
    - Representa soma matemática se utilizado com `int`s e `float`s
    - Representa concatenação se utilizado com `Array`s e `String`s
  - `-`: Subtração
    - Representa subtração matemática se utilizado com `int`s e `float`s
  - `* /`: Multiplicação / Divisão
    - Podem ser utilizados da forma esperada com `int`s e `float`s
      - Divisão entre `int`s são truncadas (arredondadas para baixo)
  - `%`: Resto
    - Representa o resto de uma divisão
    - Pode ser utilizada apenas com `int`s

```gdscript
extends Node

func _ready() -> void:
    var x: int = 10 / 3;
    var y: float = 20 * 10 + 3 * 2;

    print(x);
    print(y);
    print(y / x);
    print(100 % 30);
```

```
Output:
3
206
68.666667
10
```

- Operadores lógicos: só podem ser utilizados com valores booleanos
  
  - `&& and`: "e" lógico
  
  - `|| or`: "ou" lógico
  
  - `! not`: negação lógica

```gdscript
extends Node

func _ready() -> void:
    var eh_feriado: bool = true;
    var eh_sexta: bool = false;
    var eh_abril: bool = true;
    var eh_maio: bool = false;
    var eh_segunda: bool = true;

    print(eh_feriado and eh_segunda && eh_maio);
    print(eh_feriado || eh_abril or eh_sexta);
    print(not eh_abril);
    print(!eh_sexta);
```

```
Output:
False
True
False
True
```

- Operadores comparativos: 
  
  - `>`: Maior que
  
  - `<`: Menor que
  
  - `>=`: Maior ou igual a
  
  - `<=`: Menor ou igual a
  
  - `==`: Igual a
  
  - `!=`: Diferente de

```gdscript
extends Node

func _ready() -> void:
    var a: int = 10;
    var b: float = 10.0;
    var c: int = 100;
    var d: float = 53.45;

    print(a == b);
    print(a > b);
    print(a >= b);
    print(d <= 50);
    print(c < 100.1);
    print(c != 100);
```

```
Output:
True
False
True
False
True
False
```

- Operadores associativos:
  
  - `+=`: Soma associativa
  
  - `-=`: Subtração associativa
  
  - `*=`: Multiplicação associativa
  
  - `/=`: Divisão associativa
  
  - `%=`: Resto associativo

```gdscript
extends Node

func _ready() -> void:
    var a: int = 10;
    var b: float = 10.0;

    a += 10;
    print(a);

    b -= 100;
    print(b);

    b *= 2;
    print(b);

    a /= 3;
    print(a);

    a *= 10;
    a %= 23;
    print(a);
```

```
Output:
20
-90
-180
6
14
```

O código acima é equivalente a:

```gdscript
extends Node

func _ready() -> void:
    var a: int = 10;
    var b: float = 10.0;

    a = a + 10;
    print(a);

    b = b - 100;
    print(b);

    b = b * 2;
    print(b);

    a = a / 3;
    print(a);

    a = a * 10;
    a = a % 23;
    print(a);
```

## Controle de fluxo

- Scripts normalmente são interpretados linearmente de cima para baixo
  
  - Estruturas de controle de fluxo são responsáveis por mudar esse comportamento

### Condicional

- Executa um código caso alguma condição seja verdadeira

- A sintaxe do bloco condicional é a palavra chave `if`, seguida de uma expressão com valor booleano, seguida de `:`, seguida do código que deve ser executado quando a condição for verdadeira
  
  - Opcionalmente, pode vir um `elif` depois, com a mesma estrutura, cujo código só será executado se a expressão no `if` for falsa e a expressão no `elif` for verdadeira
  
  - Opcionalmente, pode vir um `else` depois, que não precisa de nenhuma condição, cujo código será executado se todas as condições anteriores forem falsas

```gdscript
extends Node

func _ready() -> void:
    var a: int = 10;
    var b: float = 10.0;
    var c = 10;

    if a == b:
        print("a e b são iguais");
    elif a > b:
        print("a é maior que b");
    elif a < b:
        print("a é menor que b");
    else:
        print("isso é impossível");

    if c is float: # Operador is nesse caso checa se a variável c é do tipo float
        print("c é do tipo float")
    elif c is int:
        print("c é do tipo int");
    else:
        print("c é de um tipo que não é float nem int");

    if a == b and b == c:
        print("Então a é igual a c");
```

```
Output:
a e b são iguais
c é do tipo int
Então a é igual a c
```

### Enquanto

- Executa um código em loop enquanto uma condição for verdadeira

- A condição é checada apenas ao final da execução de um loop
  
  - Caso a condição fique falsa em algum lugar no meio do loop, mas fique verdadeira depois, o loop vai continuar rodando

- A sintaxe da estrutura enquanto é a palavra chave `while`, seguida da condição de execução do loop, seugida de `:`, seguida do código que será executado enquanto a condição for verdadeira
  
  - Opcionalmente, pode ser utilizada a palavra chave `break` dentro do loop para parar a execução do loop, mesmo que a condição ainda seja verdadeira

```gdscript
extends Node

func _ready() -> void:
    var a: int = 15;

    while a < 20:
        print("a ainda é menor que 20 / Valor de a: ", a);
        a += 1;
    print("a não é mais menor que 20 / Valor de a: ", a);
    print();

    var b: float = 10000;
    while b > 1:
        print("b ainda é maior que 1 / Valor de b: ", b);
        b /= 3;
    print("b não é mais maior que 1 / Valor de b: ", b);
    print();

    var i: int = 0;
    while true:
        if i > 5:
            print("Cansei");
            break;
        print("Estou num loop infinito!");
        i += 1;
    print("Não estou mais num loop infinito");
    print();

    while true:
        print("Essa mensagem será impressa apenas uma vez");
        break;
        print("Essa mensagem nunca será mostrada");
```

```
Output:
a ainda é menor que 20 / Valor de a: 15
a ainda é menor que 20 / Valor de a: 16
a ainda é menor que 20 / Valor de a: 17
a ainda é menor que 20 / Valor de a: 18
a ainda é menor que 20 / Valor de a: 19
a não é mais menor que 20 / Valor de a: 20

b ainda é maior que 1 / Valor de b: 10000
b ainda é maior que 1 / Valor de b: 3333.333333
b ainda é maior que 1 / Valor de b: 1111.111111
b ainda é maior que 1 / Valor de b: 370.37037
b ainda é maior que 1 / Valor de b: 123.45679
b ainda é maior que 1 / Valor de b: 41.152263
b ainda é maior que 1 / Valor de b: 13.717421
b ainda é maior que 1 / Valor de b: 4.572474
b ainda é maior que 1 / Valor de b: 1.524158
b não é mais maior que 1 / Valor de b: 0.508053

Estou num loop infinito!
Estou num loop infinito!
Estou num loop infinito!
Estou num loop infinito!
Estou num loop infinito!
Estou num loop infinito!
Cansei
Não estou mais num loop infinito

Essa mensagem será impressa apenas uma vez
```

### Iterador

- Utilizado para:
  
  - Repetir um bloco de código um número determinado de vezes
  
  - Iterar por elementos de uma coleção (`Array`, `Dictionary`)

- Sintaxe da estrutura de iterador é a palavra chave `for`, seguido de um nome para cada instância do iterando, seguido da palavra chave `in`, seguido do nome da coleção que se deseja iterar
  
  - A palavra chave `break` pode ser utilizada de forma semelhante ao `while` para interromper a execução a qualquer momento

```gdscript
extends Node

func _ready() -> void:
    for i in 5:
        print(i);
    print();

    for i in range(5, 10):
        print(i);
    print();

    var alunos: Array = ["Pedro, Joana, Marcos, José, Maria, Clara, Ninguém"];
    for aluno in alunos:
        print(aluno);
        if aluno == "Clara":
            break;
    print("Note que 'Ninguém' não foi impresso");
    print();

    var disciplina: Dictionary = {
        "codigo": "MAC0422", 
        "nome": "Sistemas Operacionais",
        "professor": "Alan Mitchell Durham",
        "alunos": 43
    }
    for chave in disciplina.keys():
        print(chave, " : ", disciplina[chave]);
    print();

    for valor in disciplina.values():
        print(valor);
    print();
```

```
Output:
0
1
2
3
4

5
6
7
8
9

Pedro, Joana, Marcos, José, Maria, Clara, Ninguém
Note que 'Ninguém' não foi impresso

codigo : MAC0422
nome : Sistemas Operacionais
professor : Alan Mitchell Durham
alunos : 43

MAC0422
Sistemas Operacionais
Alan Mitchell Durham
43
```

### Correspondência

- Semelhante ao `if`, porém:
  
  - Realiza a correspondência apenas com valores constantes
  
  - É mais rápido que estruturas `if` tradicionais

- Ideal para ser usado com `enum`s

- A sintaxe de uma estrutura de correspondência é a palavra chave `match`, seguida do valor que se quer corresponder, seguida de `:`, seguida de valores contantes possíveis para o valor dado

```gdscript
extends Node

enum Tipo {
    NEUTRO,
    INIMIGO,
    ALIADO
}

func _ready() -> void:
    var entidade: int = Tipo.INIMIGO;
    match entidade:
        Tipo.NEUTRO: # Equivalente a 0
            print("Essa é uma entidade neutra");

        Tipo.INIMIGO: # Equivalente a 1
            print("Essa é uma entidade inimiga");

        Tipo.ALIADO: # Equivalente a 2
            print("Essa é uma entidade aliada");

        _: # Qualquer outro valor que não seja nenhum dos acima cai aqui
            print("Essa entidade não é de nenhum tipo conhecido");
    print();

    var numero: int = 23;
    match numero:
        0, 2, 4, 6, 8, 10:
            print("É um número par");
        1, 3, 5, 7, 9:
            print("É um número ímpar");
        var num:
            print(num, " é muito grande para eu dizer se é par ou ímpar");
    print();

    match typeof(numero):
        TYPE_REAL:
            print("É um número real (float)");
        TYPE_INT:
            print("É um número inteiro");
        TYPE_STRING:
            print("É uma String");
        _: # Não precisa ter sempre o _, mas é boa prática
            print("Não é um número inteiro nem real");
    print();

    var array: Array = [1, 2, 3, 7, 8, 9];
    match array:
        []:
            print("É um array vazio");
        [1, 2, "teste", 9]:
            print("É um array muito específico");
        [1, _, 3, ..]:
            print("O array começa com 1, tem um número qualquer, seguido de um 3, e mais coisas quaisquer no final");
```

```
Output:
Essa é uma entidade inimiga

23 é muito grande para eu dizer se é par ou ímpar
É um número inteiro

O array começa com 1, tem um número qualquer, seguido de um 3, e mais coisas quaisquer no final
```

## Funções

- Forma de extrair funcionalidades e organizar o código

- Considere o código abaixo
  
  - Em duas partes diferentes, estamos usando um iterador muito semelhante para conferir se o `Array` possui um determinado elemento

```gdscript
extends Node

func _ready() -> void:
    var alunos: Array = ["Pedro", "Joana", "Marcos", "José", "Maria", "Clara"];

    var tem_pedro: bool = false;
    for aluno in alunos:
        if aluno == "Pedro":
            tem_pedro = true;
            break;

    var tem_clara: bool = false;
    for aluno in alunos:
        if aluno == "Clara":
            tem_clara = true;
            break;
```

- Essa funcionalidade pode ser extraída em uma função

```gdscript
extends Node

func array_tem_elemento(array: Array, elemento: String) -> bool:
    for item in array:
        if item == elemento:
            return true;
    return false;

func _ready() -> void:
    var alunos: Array = ["Pedro", "Joana", "Marcos", "José", "Maria", "Clara"];
    var tem_pedro: bool = array_tem_elemento(alunos, "Pedro"); # True
    var tem_clara: bool = array_tem_elemento(alunos, "Clara"); # True
    var tem_jonas: bool = array_tem_elemento(alunos, "Jonas"); # False
```

- A função `array_tem_elemento` realiza a mesma operação que o iterador realizava, mas agora que foi extraída em uma função, pode ser reutilizada quantas vezes necessário

- Analisando a sintaxe da declaração da função:
  
  - A palavra chave `func` diz que uma função será definida naquela linha
  - Depois, o nome da função pode ser qualquer coisa, assim como o nome de uma variável
    - É boa prática dar para as funções nomes descritivos de sua funcionalidade
  - Depois, vem os parâmetros que a função recebe
    - Os parâmetros não precisam, necessariamente, ser tipados
    - Uma função não precisa receber parâmetros
  - Depois, vem o tipo de retorno da função
    - O tipo de retorno também não precisa ser, necessariamente, tipado
    - Uma função pode não retornar nada (especificado pelo tipo `void`)
  - A palavra chave `return` termina a execução da função e, se houver, retorna o valor especificado

- Alguns conceitos importantes:
  
  - `parâmetros`: são as variáveis nas quais sua função vai receber os dados necessários para sua execução
    - No exemplo acima, `array` e `elemento` são parâmetros
    - Eles ficam na declaração da função
  - `argumentos`: são os valores que sua função recebe quando são chamadas
    - No exemplo acima, `alunos`, `"Pedro"`, `"Clara"` e `"Jonas"` são argumentos
    - Eles ficam nas chamadas à função

## Escopo

- Variáveis sempre estão inseridas dentro de um escopo
  
  - Escopo global: É o escopo que não está inserido dentro de nenhuma função
  - Posso acessar variáveis de escopos externos em escopos mais internos
  - Não posso acessar variáveis de escopos internos em escopos mais externos

```gdscript
extends Node

# Escopo global
var um_numero: int = 10;

func print_um_numero() -> void:
    # Escopo interno da função print_um_numero()
    print(um_numero); # Consigo acessar essa 
    var outro_numero: int = 200;

func _ready() -> void:
    # Escopo interno da função _ready()
    um_numero = 100; # Consigo acessar essa variável em qualquer lugar
    # Não consigo acessar a variável outro_numero aqui

    if true:
        # Escopo interno do if
        var uma_string: String = "uma_string";
        if true:
            # Escopo interno do if
            uma_string = "uma string aaaaaaaaaaa";
    # Não consigo acessar a variável uma_string aqui
```

- Como é possível ver no exemplo, escopos estão muito relacionados com o nível de identação presente no código
  
  - Via de regra, se uma variável foi declarada com nível de identação `x`, ela poderá ser acessada em um nível de identação `y` se `x <= y`
    
    - Essa regra não vale se a variável foi declarada em uma função diferente
      
      - Nesse caso, não é possível acessar nunca

## Classes

- São uma forma de agrupar dados e funcionalidades semelhantes

- Considere o exemplo de querermos criar um programa que organiza carros

```gdscript
extends Node

var carro_nome: String = "Fiat Uno";
var carro_cor: String = "Azul";
var carro_velocidade_maxima: float = 223.6;
```

- Agora, digamos que queremos adicionar outro carro

```gdscript
extends Node

var carro1_nome: String = "Fiat Uno";
var carro1_cor: String = "Azul";
var carro1_velocidade_maxima: float = 120;

var carro2_nome: String = "Chevrolet Corsa";
var carro2_cor: String = "Preto";
var carro2_velocidade_maxima: float = 152.8;
```

- Se quisermos adicionar mais carros, o código vai ficar impossível de manter

- Se quisermos adicionar mais propriedades, como `altura`, também será difícil

- Podemos criar uma classe que represente o nosso `Carro`

```gdscript
extends Node

class Carro:
    var nome: String;
    var cor: String;
    var velocidade_maxima: float;
    var altura: float;

func _ready() -> void:
    var carro1 = Carro.new();
    carro1.nome = "Fiat Uno";
    carro1.cor = "Azul";
    carro1.velocidade_maxima = 150;
    carro1.altura = 1.80;
```

### Construtor

- Podemos passar argumentos iniciais para nossa classe por meio do construtor

```gdscript
extends Node

class Carro:
    var nome: String;
    var cor: String;
    var velocidade_maxima: float;
    var altura: float;

    func _init(_nome: String, _cor: String, _velocidade_maxima: float, _altura: float) -> void:
        nome = _nome;
        cor = _cor;
        velocidade_maxima = _velocidade_maxima;
        altura = _altura;

func _ready() -> void:
    var carro1 = Carro.new("Fiat Uno", "Azul", 150, 1.80);
    var carro2 = Carro.new("Chevrolet Corsa", "Preto", 180, 1.72);
```

### Métodos

- Podemos adicionar funcionalidades a nossas classes por meio de métodos

- São, essencialmente, funções dentro de classes

```gdscript
extends Node

class Carro:
    var nome: String;
    var cor: String;
    var velocidade_maxima: float;
    var altura: float;

    func _init(_nome: String, _cor: String, _velocidade_maxima: float, _altura: float) -> void:
        nome = _nome;
        cor = _cor;
        velocidade_maxima = _velocidade_maxima;
        altura = _altura;

    func imprime_nome() -> void:
        print(nome);

    func passa_por_tunel(altura_tunel: float) -> bool:
        return altura < altura_tunel;

    func mais_rapido_que(outro_carro: Carro) -> bool:
        return self.velocidade_maxima > outro_carro.velocidade_maxima;

func _ready() -> void:
    var carro1 = Carro.new("Fiat Uno", "Azul", 150, 1.80);
    var carro2 = Carro.new("Chevrolet Corsa", "Preto", 180, 1.72);

    carro1.imprime_nome();
    carro2.imprime_nome();

    print(carro1.passa_por_tunel(1.75));
    print(carro2.passa_por_tunel(1.75));
    print(carro1.mais_rapido_que(carro2));
```

```
Output:
Fiat Uno
Chevrolet Corsa
False
True
False
```

### Herança

- Podemos fazer uma classe herdar as propriedades de outras classes
  
  - Ela herda todos os métodos e propriedades
  
  - Podemos adicionar novos métodos e propriedades, ou modificar os existentes

```gdscript
extends Node

class Carro:
    var nome: String;
    var cor: String;
    var velocidade_maxima: float;
    var altura: float;

    func _init(_nome: String, _cor: String, _velocidade_maxima: float, _altura: float) -> void:
        nome = _nome;
        cor = _cor;
        velocidade_maxima = _velocidade_maxima;
        altura = _altura;

    func imprime_nome() -> void:
        print(nome);

    func passa_por_tunel(altura_tunel: float) -> bool:
        return altura < altura_tunel;

    func mais_rapido_que(outro_carro: Carro) -> bool:
        return self.velocidade_maxima > outro_carro.velocidade_maxima;

class FiatUno extends Carro:
    func _init(_cor: String, _velocidade_maxima: float, _altura: float).("FiatUno", _cor, _velocidade_maxima, _altura) -> void:
        pass

class CarroVoador extends Carro:
    var velocidade_voando: float;

    func _init(_nome: String, _cor: String, _velocidade_maxima: float, _altura: float, _velocidade_voando: float)\
    .(_nome, _cor, _velocidade_maxima, _altura) -> void:
        velocidade_voando = _velocidade_voando;

    func mais_rapido_voando_que(outro_carro_voador: CarroVoador) -> bool:
        return self.velocidade_voando > outro_carro_voador.velocidade_voando;

    func print_sei_voar() -> void:
        print("Eu sei voar!!");

func _ready() -> void:
    var fiat_uno := FiatUno.new("Azul", 150, 1.80);
    var corsa_voador := CarroVoador.new("Chevrolet Corsa", "Preto", 180, 1.72, 225);
    var fusca_voador := CarroVoador.new("Fusca", "Branco", 370, 1.90, 530);

    fiat_uno.imprime_nome();
    corsa_voador.imprime_nome();
    fusca_voador.imprime_nome();

    print(fiat_uno.passa_por_tunel(1.75));
    print(corsa_voador.passa_por_tunel(1.75));
    print(corsa_voador.passa_por_tunel(1.75));
    print(fiat_uno.mais_rapido_que(corsa_voador));
    print(fusca_voador.mais_rapido_voando_que(corsa_voador));
```

## Conteúdo extra para estudo

- [Guia básico de GDScript em português](https://docs.godotengine.org/pt_BR/stable/tutorials/scripting/gdscript/gdscript_basics.html)

- [Guia básico de GDScript em inglês](https://docs.godotengine.org/en/3.5/tutorials/scripting/gdscript/gdscript_basics.html)

obs: A trdução pt-br não é perfeita. Se souberem, sugiro que leiam a versão em inglês. Porém, caso não saibam inglês, podem usar a versão em português que funciona perferitamente.
