# Demo "bombas e explosões"

Para rodar esse demo, você precisa da Godot 4.1+.

## Rodando o demo

Abra o projeto e aperte F5 (ou clique no botão de play no canto direito
superior).

## Usando o sandbox

Abra o script "sandbox.gd" no editor da Godot e escreva seu código a partir da
linha:

```gdscript
func run(game: Game):
  # a partir daqui
```

Os comandos disponíveis são:

```gdscript

# Cria uma bomba na posição (x, y). Bombas precisam ter um nome único.
game.add_bomb_xy("nome", x, y)
game.add_bomb("nome", Vector2(x, y))

# Cria uma explosão na posição (x, y). Também precisam ter um nome único.
game.add_explosion_xy("nome", x, y)
game.add_explosion("nome", Vector2(x, y))

# Apaga objeto identificado pelo nome dado
game.remove_object("nome")

# Escreve texto na caixa de texto do demo
game.write("Texto")

# Pausa a sequência de comandos por t segundos
await game.sleep(t)

```

Você pode ver o código em "sandbox_sample.gd" para ter uma ideia do que é
possível fazer. Você pode copiar o código de lá e mudar para experimentar.
