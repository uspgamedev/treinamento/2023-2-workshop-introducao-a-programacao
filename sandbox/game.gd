class_name Game
extends Control

@export var bomb_scn: PackedScene
@export var explosion_scn: PackedScene

@onready var _space := $Space as Node2D
@onready var _sandbox := $Sandbox as Sandbox
@onready var _objs: Dictionary = {}


func add_object(id, pos: Vector2, object: Node):
	if _objs.has(id):
		remove_object(id)
	object.position = pos
	_space.add_child(object)
	_objs[id] = object


func remove_object(id):
	_objs[id].queue_free()
	_objs.erase(id)


func add_bomb(id, pos: Vector2):
	add_object(id, pos, bomb_scn.instantiate())


func add_bomb_xy(id, x: float, y: float):
	add_bomb(id, Vector2(x, y))


func add_explostion(id, pos: Vector2):
	add_object(id, pos, explosion_scn.instantiate())


func add_explosion_xy(id, x: float, y: float):
	add_explostion(id, Vector2(x, y))


func sleep(seconds: float) -> Signal:
	return get_tree().create_timer(seconds).timeout


func write(text):
	if text is String:
		$"%Output".text += text + "\n"
	elif text is Vector2:
		$"%Output".text += "%s\n" % text
	else:
		$"%Output".text += var_to_str(text) + "\n"


func _on_run_button_pressed():
	$"%Output".text = ""
	for id in _objs:
		_objs[id].queue_free()
	_objs.clear()
	await get_tree().process_frame
	_sandbox.run(self)
