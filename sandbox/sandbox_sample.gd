class_name SandboxSample
extends Sandbox

const origin := Vector2(64, 64)
const dist := 128


func define_bomb_positions(bomb_count: int) -> Dictionary:
	var bomb_positions := {}
	
	for counter in bomb_count:
		var bomb_name := "bomb" + var_to_str(counter)
		bomb_positions[bomb_name] = \
			origin + Vector2.DOWN * dist * counter
	
	return bomb_positions


func run(game: Game):
	var bomb_count := 3
	var bomb_positions := define_bomb_positions(bomb_count)
	for bomb_name in bomb_positions:
		game.add_bomb(
			bomb_name, bomb_positions[bomb_name]
		)
	await game.sleep(2.0)
	var counter := 0
	for bomb_name in bomb_positions:
		game.remove_object(bomb_name)
		game.add_explostion(
			"explo" + var_to_str(counter),
			bomb_positions[bomb_name]
		)
		counter += 1
	game.write("end")
